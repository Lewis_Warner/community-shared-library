# @warnster/shared-library

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/@warnster/shared-library.svg)](https://www.npmjs.com/package/@warnster/shared-library) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @warnster/shared-library
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from '@warnster/shared-library'
import '@warnster/shared-library/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)

## Publish

to publish this package run
npm publish --access=public
