/* eslint-disable @typescript-eslint/no-unused-vars */
import { IPostFilter } from '@warnster/community-shared-interfaces'
import { PostClientDao, usePosts } from '@warnster/community-shared-library'
import React, { useEffect, useState } from 'react'
require('dotenv').config()
const Post = () => {
  const [postFilter, setPostFilter] = useState<IPostFilter>({
    limit: 3
  })
  const [trigger, setTrigger] = useState(false)
  const { posts, allPosts, lastVisible } = usePosts({
    trigger,
    filters: postFilter
  })

  useEffect(() => {
    const postDao = new PostClientDao()
    // postDao.getPosts(postFilter).then(({ posts, lastPost }) => {
    //   setLastVisiblePost(lastPost)
    // })
    console.log({ posts, allPosts })
  }, [postFilter, posts, allPosts])

  return (
    <div>
      <button
        onClick={() => {
          console.log({ lastVisible })
          setPostFilter({ ...postFilter, startAfter: lastVisible })

          setTrigger(!trigger)
        }}
      >
        Next Posts
      </button>
    </div>
  )
}

export default Post
