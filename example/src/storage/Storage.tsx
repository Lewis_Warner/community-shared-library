import { StorageDaoClient } from '@warnster/community-shared-library'
import React from 'react'

export const StorageTest = () => {
  const storage = new StorageDaoClient()

  const jpegFile = new File(['foo'], 'foo.txt', {
    type: 'text/plain'
  })

  const fileUpload = async () => {
    console.log(process.env)
    const result = await storage.uploadFile(jpegFile, '/tests', (task) => {
      console.log({ task })
    })
    console.log({ result })
  }

  return (
    <button type='button' onClick={fileUpload}>
      Start File Upload Test
    </button>
  )
}
