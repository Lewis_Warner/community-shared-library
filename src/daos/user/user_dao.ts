import {
  IMessageGroup,
  IUser,
  IUserRetrievedFirebase,
  IUserUpdate,
  IUserUpdateDraft
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { transformFirebaseUser } from '../../helpers/user/user_helper'
import Firebase from '../firebase/firebase'

export class UserDao extends Firebase {
  private usersCollection(): firebase.firestore.CollectionReference<firebase.firestore.DocumentData> {
    return this.db.collection('users')
  }

  public async updateUser(
    userID: string,
    updateDataDraft: IUserUpdateDraft
  ): Promise<void> {
    const updateData: IUserUpdate = {
      ...updateDataDraft,
      updatedAt: this.firebase.firestore.FieldValue.serverTimestamp()
    }
    return this.usersCollection().doc(userID).update(updateData)
  }

  public async removeUserFromGroup(
    group: IMessageGroup,
    userID: string
  ): Promise<void> {
    return this.updateUser(userID, {
      groups: this.firebase.firestore.FieldValue.arrayRemove(group.id)
    })
  }

  public async addUserToGroup(groupID: string, userID: string): Promise<void> {
    return this.updateUser(userID, {
      groups: this.firebase.firestore.FieldValue.arrayUnion(groupID)
    })
  }

  public getUsers(
    userIDs: string[]
  ): firebase.firestore.Query<firebase.firestore.DocumentData> {
    return this.usersCollection().where('uid', 'in', userIDs)
  }

  public getUser(
    userID: string
  ): firebase.firestore.DocumentReference<firebase.firestore.DocumentData> {
    return this.usersCollection().doc(userID)
  }

  public async getUserType(userID: string): Promise<IUser> {
    const doc = await this.getUser(userID).get()
    if (!doc.exists) {
      throw new Error("user doc doesn't exist")
    }
    const user = transformFirebaseUser(doc.data() as IUserRetrievedFirebase)
    return user
  }

  public async findUserByID(userID: string): Promise<IUser | null> {
    try {
      const result = await this.getUser(userID).get()
      if (result.exists) {
        return transformFirebaseUser(result.data() as IUserRetrievedFirebase)
      } else {
        return null
      }
    } catch (err) {
      console.error('findUserByID', { err })
      return null
    }
  }

  public onUserUpdate(
    userID: string,
    onUpdate: (user: IUser) => void
  ): () => void {
    return this.getUser(userID).onSnapshot(
      (querySnapshot) => {
        const user = transformFirebaseUser(
          querySnapshot.data() as IUserRetrievedFirebase
        )
        onUpdate(user)
      },
      (error) => {
        console.error({ error })
      }
    )
  }
}
