import { StorageDaoClient } from '../storage'
globalThis.Blob = Blob
describe('Storage Dao Integration', () => {
  const storageDao = new StorageDaoClient()
  it('storage should be available', () => {
    expect(storageDao.storageAvailable).toEqual(true)
  })

  it('should upload a file', async () => {
    const file = new File([new ArrayBuffer(1)], 'file.jpg')

    try {
      console.log({ blob })
      const result = await storageDao.uploadFile(file, '/test', (task) => {})
      console.log({ result })
    } catch (err) {
      console.log({ err })
    }
  })
})
