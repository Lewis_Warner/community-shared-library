import firebase from 'firebase'
import Firebase from './firebase'

// Firestore uses a different server timestamp value, so we'll
// create two more constants for Firestore state.
// const isOfflineForFirestore = {
//   state: 'offline',
//   last_changed: firebase.firestore.FieldValue.serverTimestamp()
// }

const isOnlineForFirestore: any = {
  state: 'online',
  last_changed: firebase.firestore.FieldValue.serverTimestamp()
}

const isOfflineForDatabase = {
  state: 'offline',
  last_changed: firebase.database.ServerValue.TIMESTAMP
}

const isOnlineForDatabase = {
  state: 'online',
  last_changed: firebase.database.ServerValue.TIMESTAMP
}

export class RealtimeDao extends Firebase {
  realtime: firebase.database.Database
  constructor() {
    super()
    this.realtime = this.firebase.database()
  }

  public async monitorOnlineStatus(
    uid: string,
    profileID: string,
    long?: number,
    lat?: number
  ): Promise<void> {
    const userStatusDatabaseRef = firebase.database().ref('/status/' + uid)
    const userStatusFirestoreRef = firebase
      .firestore()
      .doc('/profiles/' + profileID)
    firebase
      .database()
      .ref('.info/connected')
      .on('value', function (snapshot) {
        // If we're not currently connected, don't do anything.
        if (snapshot.val() == false) {
          // userStatusFirestoreRef.update(isOfflineForFirestore);
          return
        }

        // If we are currently connected, then use the 'onDisconnect()'
        // method to add a set which will only trigger once this
        // client has disconnected by closing the app,
        // losing internet, or any other means.
        userStatusDatabaseRef
          .onDisconnect()
          .set(isOfflineForDatabase)
          .then(function () {
            // The promise returned from .onDisconnect().set() will
            // resolve as soon as the server acknowledges the onDisconnect()
            // request, NOT once we've actually disconnected:
            // https://firebase.google.com/docs/reference/js/firebase.database.OnDisconnect

            // We can now safely set ourselves as 'online' knowing that the
            // server will mark us as offline once we lose connection.
            userStatusDatabaseRef.set(isOnlineForDatabase)
            if (long && lat) {
              isOnlineForFirestore['location.coordinates'] = [long, lat]
              isOnlineForFirestore['location.type'] = 'Point'
            }
            userStatusFirestoreRef.update(isOnlineForFirestore)
          })
      })
  }

  public async getUserOnlineStatus(
    uid: string,
    callback: (snapshot: firebase.database.DataSnapshot) => void
  ): Promise<void> {
    this.realtime.ref('/status/' + uid).on('value', callback)
  }

  public async setUserOffline(uid: string): Promise<void> {
    const userStatusDatabaseRef = firebase.database().ref('/status/' + uid)
    userStatusDatabaseRef.ref.set(isOfflineForDatabase)
  }
}
