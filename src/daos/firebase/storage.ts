import firebase from 'firebase'
import { v4 as uuid } from 'uuid'
import { IFileDisplay } from '../../'
import Firebase from './firebase'

export class StorageDaoClient extends Firebase {
  protected storage: firebase.storage.Storage
  public storageAvailable: boolean
  constructor() {
    super()
    // Add check as it can only be invoke on client side
    if (this.firebase && this.firebase.storage) {
      this.storage = this.firebase.storage()
      this.storageAvailable = true
    } else {
      this.storageAvailable = false
    }
  }

  private getStorage(): firebase.storage.Storage {
    return this.storage as firebase.storage.Storage
  }

  public getStorageRef(path: string): firebase.storage.Reference {
    return this.getStorage().ref(path)
  }

  public getFileReference(path: string): firebase.storage.Reference {
    return this.getStorage().ref().child(path)
  }

  // Handle waiting to upload each file using promise
  // if blob is passed then a file extensino is needed
  public async uploadFile(
    file: File | IFileDisplay | Blob,
    storagePath: string,
    progress: (task: firebase.storage.UploadTaskSnapshot) => void,
    ext?: string
  ): Promise<firebase.storage.UploadTask> {
    return new Promise(function (resolve, reject) {
      const fileName = uuid()
      let fileExtension: string
      if (file instanceof File) {
        fileExtension = file.name.split('.').pop() as string
      } else {
        fileExtension = ext as string
      }
      const storageRef = firebase
        .storage()
        .ref(`${storagePath}/${fileName}.${fileExtension}`)
      // Upload file
      const task = storageRef.put(file)
      // Update progress bar
      task.on(
        'state_changed',
        progress,
        (error) => {
          reject(new Error(error.message))
        },
        () => {
          resolve(task)
        }
      )
    })
  }

  public async deleteFile(storagePath: string): Promise<any> {
    const storageRef = firebase.storage().ref(storagePath)
    return storageRef.delete()
  }
}
