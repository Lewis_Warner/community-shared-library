import {
  ICheckoutSessionItem,
  IFirebaseCreatePortalResponse,
  IPlanDetails,
  IProduct,
  IProductDetails,
  IProductPrice,
  IProductUsage
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { CurrencyToSymbol } from '../../helpers/user/user_helper'
import Firebase from '../firebase/firebase'

export const PRICING_DETAILS = {
  FREE_TIER_PRODUCT_ID: 'prod_JKfC6UzXl2b25L',
  FREE_TIER_PRICE_ID: 'price_1IhzsGHbwo7cK3xkr7pdbWXz',
  PAID_TIER_PRICE_ID: 'price_1IhzvWHbwo7cK3xkrYhJQIFR'
}

export class Stripe extends Firebase {
  private getFirebase(): typeof firebase {
    return this.firebase as typeof firebase
  }

  private getFirestore(): firebase.firestore.Firestore {
    return this.getFirebase().firestore()
  }

  public async addCheckoutItem(
    userID: string,
    item: ICheckoutSessionItem
  ): Promise<
    firebase.firestore.DocumentReference<firebase.firestore.DocumentData>
  > {
    return this.getFirestore()
      .collection('users')
      .doc(userID)
      .collection('checkout_sessions')
      .add(item)
  }

  public async getCustomerPortalUrl(): Promise<IFirebaseCreatePortalResponse> {
    const functionRef = this.getFirebase()
      .app()
      .functions('europe-west2')
      .httpsCallable('ext-firestore-stripe-subscriptions-createPortalLink')
    const { data }: { data: IFirebaseCreatePortalResponse } = await functionRef(
      {
        returnUrl: window.location.origin
      }
    )
    return data
  }

  public getUsersSubscription(
    userID: string
  ): firebase.firestore.Query<firebase.firestore.DocumentData> {
    return this.getFirestore()
      .collection('users')
      .doc(userID)
      .collection('subscriptions')
      .where('status', '==', 'active')
  }

  private getFreeProduct(): firebase.firestore.DocumentReference<firebase.firestore.DocumentData> {
    return this.getFirestore()
      .collection('products')
      .doc(PRICING_DETAILS.FREE_TIER_PRODUCT_ID)
  }

  public async getFreePlan(): Promise<IPlanDetails> {
    const snapshot = await this.getFreeProduct().get()
    const priceSnapshot = await this.getFreeProduct()
      .collection('prices')
      .doc(PRICING_DETAILS.FREE_TIER_PRICE_ID)
      .get()
    const productUsageSnapshot = await this.getProductUsages()
      .doc(PRICING_DETAILS.FREE_TIER_PRODUCT_ID)
      .get()
    if (snapshot.exists && priceSnapshot.exists) {
      const product = snapshot.data() as IProduct
      const price = priceSnapshot.data() as IProductPrice
      let usage = {}
      if (productUsageSnapshot.exists) {
        usage = productUsageSnapshot.data() as IProductUsage
      }
      return {
        currency: CurrencyToSymbol[price.currency],
        price: price.unit_amount,
        interval: price.interval,
        image: product.images[0],
        name: product.name,
        usage
      }
    }
    throw Error('Free Subscription not available')
  }

  public getProducts(): firebase.firestore.Query<firebase.firestore.DocumentData> {
    return this.getFirestore()
      .collection('products')
      .where('active', '==', true)
  }

  public getProductUsages(): firebase.firestore.CollectionReference<firebase.firestore.DocumentData> {
    return this.getFirestore().collection('productUsage')
  }

  public async getProductDetails(): Promise<IProductDetails[]> {
    const productDocs = await this.getProducts().get()
    const products: { [key: string]: IProduct } = {}
    productDocs.docs.forEach((doc) => {
      products[doc.id] = doc.data() as IProduct
    })
    const productUsageDocs = await this.getProductUsages().get()
    const productUsages: { [key: string]: IProductUsage } = {}
    productUsageDocs.docs.forEach((doc) => {
      productUsages[doc.id] = doc.data() as IProductUsage
    })

    const productDetails: IProductDetails[] = []
    let index = 0
    for (const [productID, product] of Object.entries(products)) {
      let hasUsagePlan = false
      let productUsage = {}
      if (productUsages[productID]) {
        hasUsagePlan = true
        productUsage = productUsages[productID]
      }
      productDetails.push({
        ...product,
        hasUsagePlan,
        ...productUsage,
        productID,
        id: index
      })
      index++
    }

    return productDetails
  }

  public async createProductUsage(
    productID: string,
    productUsage: IProductUsage
  ): Promise<void> {
    return this.getFirestore()
      .collection('productUsage')
      .doc(productID)
      .set(productUsage)
  }

  public async updateProductUsage(
    productID: string,
    productUsage: IProductUsage
  ): Promise<void> {
    return this.getFirestore()
      .collection('productUsage')
      .doc(productID)
      .update(productUsage)
  }
}
