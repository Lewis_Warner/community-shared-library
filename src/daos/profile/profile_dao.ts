import {
  IProfile,
  IProfileFilters,
  IProfileFirebase,
  IProfileUpdate,
  IProfileUpdateDraft
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { transformFirebaseProfile } from '../../helpers/user/user_helper'
import Firebase from '../firebase/firebase'

export class ProfileDao extends Firebase {
  public profiles(): firebase.firestore.CollectionReference<firebase.firestore.DocumentData> {
    return this.firebase.firestore().collection('profiles')
  }

  public async updateProfile(
    profile: IProfileUpdateDraft,
    porfileID: string
  ): Promise<any> {
    const profileUpdate: IProfileUpdate = {
      ...profile,
      updatedAt: this.firebase.firestore.FieldValue.serverTimestamp()
    }
    return this.profiles().doc(porfileID).update(profileUpdate)
  }

  public async getProfileByUserID(userID: string): Promise<IProfile> {
    const snapshot = await this.profiles().where('userID', '==', userID).get()
    if (snapshot.docs.length === 0) {
      throw new Error('Profile Document for ' + userID + ' doesnt exist')
    }
    const doc = snapshot.docs[0].data()
    const docID = snapshot.docs[0].id

    const profile = await transformFirebaseProfile(
      doc as IProfileFirebase,
      docID
    )
    return profile
  }

  public async getProfiles({
    ...filters
  }: IProfileFilters): Promise<IProfile[]> {
    if (filters.startAt && filters.startAfter) {
      throw Error("can't set both startAt and startAfter")
    }
    let query = this.profiles()
      .where('isComplete', '==', true)
      .limit(filters.limit)
    if (filters.filters.excludeUser) {
      query = query.where('userID', '!=', filters.filters.excludeUser)
    }
    // dynamic filters
    if (filters.filters.country) {
      query = query.where('country', '==', filters.filters.country)
    }
    if (filters.filters.county) {
      query = query.where('county', '==', filters.filters.county)
    }
    if (filters.filters.genre) {
      query = query.where('genre', '==', filters.filters.genre)
    }
    if (filters.filters.gender) {
      query = query.where(`gender`, '==', filters.filters.gender)
    }
    if (filters.filters.ethnicity) {
      query = query.where('ethnicity', '==', filters.filters.ethnicity)
    }

    if (filters.startAt) {
      query = query.startAt(filters.startAt)
    }
    if (filters.startAfter) {
      query = query.startAfter(filters.startAfter)
    }
    if (filters.orderColumn && filters.order) {
      query = query.orderBy(filters.orderColumn, filters.order)
    }
    const snapshot = await query.get()
    const profiles: IProfile[] = []
    for (let i = 0; i < snapshot.docs.length; i++) {
      const doc = snapshot.docs[i]
      profiles.push(
        await transformFirebaseProfile(doc.data() as IProfileFirebase, doc.id)
      )
    }
    return profiles
  }

  public async getProfileByID(profileID: string): Promise<IProfile | null> {
    const snapshot = await this.profiles().doc(profileID).get()
    let profile: IProfile | null = null
    if (snapshot.exists) {
      profile = await transformFirebaseProfile(
        snapshot.data() as IProfileFirebase,
        snapshot.id
      )
    }
    return profile
  }
}
