import firebase from 'firebase'
import { ProfileDao } from './profile_dao'

export class ProfileDaoClient extends ProfileDao {
  private getFirebase(): typeof firebase {
    return this.firebase as typeof firebase
  }

  private getFirestore(): firebase.firestore.Firestore {
    return this.getFirebase().firestore()
  }

  public getAuth(): firebase.auth.Auth {
    return this.getFirebase().auth()
  }
}
