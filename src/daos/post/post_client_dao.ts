import {
  IComment,
  ICommentFilter,
  IPost,
  IPostFilter
} from '@warnster/community-shared-interfaces'
import { PostDao } from './post_dao'
export class PostClientDao extends PostDao {
  public onPostComment(
    filter: ICommentFilter,
    onUpdate: (comments: IComment[]) => void
  ): () => void {
    let commentQuery = this.comments().limit(filter.limit)
    commentQuery = this.applyCommentQuery(commentQuery, filter)
    return commentQuery.onSnapshot((snapshot) => {
      const comments = this.getCommentsFromSnapshot(snapshot)
      onUpdate(comments)
    })
  }

  public onPost(
    filter: IPostFilter,
    onUpdate: (posts: IPost[]) => void
  ): () => void {
    let postQuery = this.posts().limit(filter.limit)
    postQuery = this.applyPostFilters(postQuery, filter)
    return postQuery.onSnapshot((snapshot) => {
      const posts = this.getPostsFromSnapshot(snapshot)
      onUpdate(posts)
    })
  }
}
