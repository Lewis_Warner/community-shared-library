import {
  EGroupType,
  IMessageCreate,
  IMessageFilters,
  IMessageGroupCreate,
  IMessageGroupRetrievedFirebase,
  IMessageGroupUpdate
} from '@warnster/community-shared-interfaces'
import { uniqueUID } from '../../../helpers/utils'
import { config } from '../../firebase/firebase'
import { ChatDao } from '../chat'
describe('Chat Dao Integration', () => {
  it('should get config', () => {
    expect(config.apiKey).toBeTruthy()
  })
  const chatDao = new ChatDao()
  const groupId = 'ogAuhyFwoFyjSnBoFd8Z'
  const message: IMessageCreate = {
    id: uniqueUID(),
    senderId: 'testSenderId',
    body: 'test message body',
    contentType: 'text',
    createdAt: chatDao.getServerTime()
  }
  const userId1 = 'testUserId1'
  xit('should create a group', async () => {
    const groupData: IMessageGroupCreate = {
      avatar: '',
      createdBy: '',
      members: {
        [userId1]: {
          name: 'testName',
          avatar: '',
          isRemoved: false,
          isDeleted: false
        }
      },
      name: 'group name',
      type: EGroupType.PrivateChat,
      unread: {
        [userId1]: 0
      },
      createdAt: chatDao.getServerTime(),
      updatedAt: chatDao.getServerTime(),
      recentMessage: message
    }
    const groupRef = await chatDao.createGroup(groupData)
    const group = await groupRef.get()
    const result = {
      ...group.data(),
      createdAt: null,
      updatedAt: null,
      recentMessage: {
        ...group.data()?.recentMessage,
        createdAt: null,
        updatedAt: null
      }
    }
    const groupDataNull: any = { ...groupData }
    groupDataNull.createdAt = null
    groupDataNull.updatedAt = null
    groupDataNull.recentMessage.createdAt = null
    groupDataNull.recentMessage.updatedAt = null
    expect(result).toEqual(groupDataNull) //
  })

  xit('should update group after message', async () => {
    const groupData: IMessageGroupUpdate = {
      id: groupId,
      updatedAt: chatDao.getServerTime(),
      createdAt: new Date(),
      recentMessage: message
    }
    const preTestGroup = (await (
      await chatDao.getGroups([groupData.id]).get()
    ).docs[0].data()) as IMessageGroupRetrievedFirebase
    const preTestUnread = preTestGroup.unread[userId1]
    await chatDao.updateGroupAfterMessage(groupData, [userId1])
    const postTestGroup = (await (
      await chatDao.getGroups([groupData.id]).get()
    ).docs[0].data()) as IMessageGroupRetrievedFirebase
    const postTestUnread = postTestGroup.unread[userId1]
    expect(postTestUnread).toEqual(preTestUnread + 1)
  })

  xit('should add a message', async () => {
    const messageRef = await chatDao.addMessage(message, groupId)
    const messageResult = {
      ...(await messageRef.get()).data(),
      createdAt: null
    }
    expect(messageResult).toEqual({ ...message, createdAt: null })
  })

  xit('should get paginated message', async () => {
    const limit = 1
    const filter: IMessageFilters = {
      filters: {
        groupID: groupId
      },
      limit,
      orderColumn: 'createdAt',
      order: 'desc'
    }
    //Tests if the limit filter works
    const { messages, startAt } = await chatDao.getPaginatedMessages(filter)
    expect(messages.length).toEqual(limit)

    //Tests if the startAfter filter and order works
    const { messages: messages2 } = await chatDao.getPaginatedMessages({
      ...filter,
      startAfter: startAt
    })
    expect(messages[0].createdAt > messages2[0].createdAt)
    expect(messages[0].id).not.toEqual(messages2[0].id)
    const { messages: messages3 } = await chatDao.getPaginatedMessages({
      ...filter,
      startAt
    })
    //Test the startAt
    expect(messages3[0].id).toEqual(messages[0].id)
  })
})
