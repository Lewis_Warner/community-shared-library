import firebase from 'firebase'
import { ChatDao } from './chat'

export class ChatDaoClient extends ChatDao {
  private getFirebase(): typeof firebase {
    return this.firebase as typeof firebase
  }

  private getFirestore(): firebase.firestore.Firestore {
    return this.getFirebase().firestore()
  }

  public getGroups(
    groupIDs: string[]
  ): firebase.firestore.Query<firebase.firestore.DocumentData> {
    return super.getGroups(
      groupIDs
    ) as firebase.firestore.Query<firebase.firestore.DocumentData>
  }
}
