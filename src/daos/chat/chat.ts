import {
  IMessage,
  IMessageCreate,
  IMessageFilters,
  IMessageGroupCreate,
  IMessageGroupUpdate,
  IMessageRetrievedFirebase
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { transformFirebaseMessage } from '../../helpers/chat/chat_helper'
import Firebase from '../firebase/firebase'

export class ChatDao extends Firebase {
  public async createGroup(
    group: IMessageGroupCreate
  ): Promise<
    firebase.firestore.DocumentReference<firebase.firestore.DocumentData>
  > {
    const groupRef = this.db.collection('groups')
    return groupRef.add(group)
  }

  public async updateGroupAfterMessage(
    group: IMessageGroupUpdate,
    recipientIDs: string[]
  ): Promise<void> {
    const updateGroup: any = {
      ...group
    }
    recipientIDs.forEach((id) => {
      updateGroup['unread.' + id] = this.increment(1)
    })
    return this.updateGroup(group.id, updateGroup)
  }

  protected groupCollection(): firebase.firestore.CollectionReference<firebase.firestore.DocumentData> {
    return this.db.collection('groups')
  }

  public updateGroup(groupID: string, updateData: any): Promise<void> {
    return this.groupCollection().doc(groupID).update(updateData)
  }

  public messageCollection(
    groupID: string
  ): firebase.firestore.CollectionReference<firebase.firestore.DocumentData> {
    return this.db.collection('messages').doc(groupID).collection('messages')
  }

  public async addMessage(
    message: IMessageCreate,
    groupID: string
  ): Promise<
    firebase.firestore.DocumentReference<firebase.firestore.DocumentData>
  > {
    return this.messageCollection(groupID).add(message)
  }

  public async getPaginatedMessages(
    filters: IMessageFilters
  ): Promise<{
    messages: IMessage[]
    startAt: firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>
  }> {
    if (filters.startAt && filters.startAfter) {
      throw Error("can't set both startAt and startAfter")
    }
    let query = this.messageCollection(filters.filters.groupID).limit(
      filters.limit
    )

    if (filters.orderColumn && filters.order) {
      query = query.orderBy(filters.orderColumn, filters.order)
    }
    if (filters.startAt) {
      query = query.startAt(filters.startAt)
    }
    if (filters.startAfter) {
      query = query.startAfter(filters.startAfter)
    }
    const snapshot = await query.get()
    const startAt = snapshot.docs[snapshot.docs.length - 1]
    const messages: IMessage[] = []
    for (let i = 0; i < snapshot.docs.length; i++) {
      const doc = snapshot.docs[i]
      messages.push(
        await transformFirebaseMessage(doc.data() as IMessageRetrievedFirebase)
      )
    }
    return { messages, startAt }
  }

  // clear
  public getGroups(
    groupIDs: string[]
  ): firebase.firestore.Query<firebase.firestore.DocumentData> {
    return this.groupCollection()
      .where('id', 'in', groupIDs)
      .orderBy('updatedAt', 'desc')
  }

  public async updateGroupUnread(
    groupID: string,
    userID: string
  ): Promise<void> {
    const key = `unread.${userID}`
    const update = {
      [key]: 0
    }
    return this.updateGroup(groupID, update)
  }

  public async deleteGroup(groupID: string): Promise<void> {
    return this.groupCollection().doc(groupID).delete()
  }
}
