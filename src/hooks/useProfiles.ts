import { IProfile, IUseProfiles } from '@warnster/community-shared-interfaces'
import { useEffect, useState } from 'react'
import { ProfileDaoClient } from '../daos/profile/profile_client_dao'

export const useProfiles = ({
  trigger,
  ...filters
}: IUseProfiles): {
  isLoading: boolean
  profiles: IProfile[]
} => {
  const [isLoading, setIsLoading] = useState(true)
  const [profiles, setProfiles] = useState<IProfile[]>([])
  const profileDao = new ProfileDaoClient()

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      setIsLoading(true)
      const profiles = await profileDao.getProfiles(filters)
      setProfiles(profiles)
      setIsLoading(false)
    }
    fetchData()
  }, [trigger])

  return { isLoading, profiles }
}
