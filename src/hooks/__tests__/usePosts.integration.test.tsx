import { act, renderHook } from '@testing-library/react-hooks'
import { usePosts } from '../usePosts'

describe('usePosts Integration Tests', () => {
  it('get posts', () => {
    const limit = 1
    const { result } = renderHook((postFilters) => usePosts(postFilters), {
      initialProps: {
        trigger: false,
        filters: {
          limit
        }
      }
    })

    act(() => {
      expect(result.current.posts.length).toBe(limit)
    })
  })
})
