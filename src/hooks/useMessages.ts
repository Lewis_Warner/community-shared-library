import { IMessage, IUseMessages } from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ChatDaoClient } from '../daos/chat/chat_client_dao'
import { getFormattedMessages } from '../helpers/chat/chat_helper'
import { IAppState } from '../interfaces/settings_interface'
import {
  appendMessage,
  prependMessages,
  setMessages
} from '../store/actions/chatActions'

export const useMessages = ({
  trigger,
  ...filters
}: IUseMessages): {
  isLoading: boolean
  messages: IMessage[]
  messageChunk: IMessage[]
  startAt:
    | firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>
    | undefined
  isLoadingMore: boolean
  hasMoreMessages: boolean
} => {
  const [hasMoreMessages, setHasMoreMessages] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const dispatch = useDispatch()
  const [isLoadingMore, setIsLoadingMore] = useState(false)
  const { chat, account } = useSelector((state: IAppState) => state)
  const [messageChunk, setMessageChunk] = useState<IMessage[]>([])
  const { messages } = chat
  const [inistalLoad, setInitialLoad] = useState(false)
  const [startAt, setStartAt] = useState<
    firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>
  >()
  const chatDao = new ChatDaoClient()

  useEffect(() => {
    if (!inistalLoad) return
    const listener = chatDao
      .messageCollection(filters.filters.groupID)
      .orderBy('createdAt', 'desc')
      .limit(1)
      .onSnapshot(async (querySnapshot) => {
        const formattedMessages = await getFormattedMessages(querySnapshot)
        const message = formattedMessages[0]
        if (message.id !== messages[0].id) {
          dispatch(
            appendMessage(message, filters.filters.groupID, account.user.uid)
          )
        }
      })
    return (): void => {
      listener()
    }
  }, [inistalLoad])

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      if (messages.length !== 0) {
        setIsLoadingMore(true)
      } else {
        setIsLoading(true)
      }
      const {
        messages: newMessages,
        startAt
      } = await chatDao.getPaginatedMessages(filters)
      if (newMessages.length === filters.limit) {
        newMessages.pop()
        setHasMoreMessages(true)
      } else {
        setHasMoreMessages(false)
      }
      setMessageChunk(newMessages)
      if (filters.startAt) {
        dispatch(prependMessages(newMessages.reverse()))
      } else {
        dispatch(setMessages(newMessages.reverse()))
      }
      setStartAt(startAt)
      setInitialLoad(true)
      setIsLoading(false)
      setIsLoadingMore(false)
    }
    fetchData()
  }, [trigger])

  return {
    isLoading,
    messages,
    messageChunk,
    startAt,
    isLoadingMore,
    hasMoreMessages
  }
}
