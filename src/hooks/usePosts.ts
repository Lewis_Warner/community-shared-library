import { IPost, IUsePosts } from '@warnster/community-shared-interfaces'
import { useEffect, useState } from 'react'
import { PostClientDao } from '../daos/post/post_client_dao'

export const usePosts = ({
  trigger,
  filters
}: IUsePosts): {
  isLoading: boolean
  posts: IPost[]
  lastVisible: any
  allPosts: IPost[]
} => {
  const [posts, setPosts] = useState<IPost[]>([])
  const [allPosts, setAllPosts] = useState<IPost[]>([])
  const [isLoading, setIsLoading] = useState(true)
  const [lastVisible, setLastVisible] = useState<any>()

  useEffect(() => {
    const postDao = new PostClientDao()
    const initialFilter = { ...filters, limit: 1 }
    const listener = postDao.onPost(initialFilter, (newPosts) => {
      setAllPosts((allPosts) => [...newPosts, ...allPosts])
      setPosts(newPosts)
    })
    return (): void => {
      listener()
    }
  }, [])

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      //stops fetching if there are posts and lastVisible is undefined.
      //This means a request from the lastVisible returned nothing previously and there are no more posts to get.
      if (lastVisible === undefined && allPosts.length > 0) {
        return
      }
      setIsLoading(true)
      const postDao = new PostClientDao()
      const { posts, lastVisible: lastVisiblePost } = await postDao.getPosts(
        filters
      )
      setLastVisible(lastVisiblePost)
      setAllPosts([...allPosts, ...posts])
      setPosts(posts)
      setIsLoading(false)
    }
    fetchData()
  }, [trigger])

  return { isLoading, posts, lastVisible, allPosts }
}
