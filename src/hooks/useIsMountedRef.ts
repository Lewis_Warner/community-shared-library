import { useEffect, useRef } from 'react'

export default function useIsMountedRef(): React.MutableRefObject<boolean> {
  const isMounted = useRef(true)

  useEffect(
    () => (): void => {
      isMounted.current = false
    },
    []
  )

  return isMounted
}

export function usePrevious(value: any): undefined {
  const ref = useRef()
  useEffect(() => {
    ref.current = value
  })
  return ref.current
}
