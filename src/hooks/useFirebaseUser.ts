import firebase from 'firebase'
import { useEffect, useState } from 'react'
import { UserDaoClient } from '../daos/user/user_client_dao'

export const useFirebaseUser = (): {
  isLoading: boolean
  firebaseUser: firebase.User | undefined
} => {
  const [isLoading, setIsLoading] = useState(true)
  const [firebaseUser, setFirebaseUser] = useState<firebase.User>()
  const userDao = new UserDaoClient()

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      setIsLoading(true)
      const data = await userDao.getCurrentUser()
      if (data) {
        setFirebaseUser(data)
      }
      setIsLoading(false)
    }
    fetchData()
  }, [])

  return { isLoading, firebaseUser }
}
