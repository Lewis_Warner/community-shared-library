import { IComment, ICommentFilter } from '@warnster/community-shared-interfaces'
import { useEffect, useState } from 'react'
import { PostClientDao } from '../daos/post/post_client_dao'

export const useComments = (
  commentFilter: ICommentFilter,
  postID: string
): {
  comments: IComment[]
} => {
  const [comments, setComments] = useState<IComment[]>([])

  useEffect(() => {
    // listens for new comments
    const postDao = new PostClientDao()
    const commentListener = postDao.onPostComment(commentFilter, (comments) => {
      setComments(comments)
    })
    return (): void => {
      commentListener()
    }
  }, [postID])

  return { comments }
}
