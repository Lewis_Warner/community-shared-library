import {
  EGroupType,
  IMessage,
  IMessageGroup,
  IMessageGroupRetrievedFirebase,
  IMessageRetrievedFirebase
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { uniqueUID } from '../helpers/utils'

export const TEST_USER_ID_1 = 'testUserId1'
export const TEST_USER_ID_2 = 'testUserId2'
export const mockFirebaseTimestamp = (
  date: Date
): firebase.firestore.Timestamp => {
  return new firebase.firestore.Timestamp(date.getTime() / 1000, 0)
}

const baseGroup = {
  avatar: 'testGroupAvatar',
  createdBy: '',
  members: {
    [TEST_USER_ID_1]: {
      name: 'testName',
      avatar: 'testAvatar1',
      isRemoved: false,
      isDeleted: false
    },
    [TEST_USER_ID_2]: {
      name: 'testName2',
      avatar: 'testAvatar2',
      isRemoved: false,
      isDeleted: false
    }
  },
  name: 'group name',
  type: EGroupType.PrivateChat,
  unread: {
    [TEST_USER_ID_1]: 0,
    [TEST_USER_ID_2]: 0
  }
}

const testDate = new Date()

export const messageRetrievedFirebase = (
  date: Date
): IMessageRetrievedFirebase => {
  return {
    id: uniqueUID(),
    body: 'test',
    senderId: '',
    contentType: 'text',
    createdAt: mockFirebaseTimestamp(date)
  }
}

export const getMessage = (date: Date): IMessage => {
  return {
    id: uniqueUID(),
    body: 'test',
    senderId: '',
    contentType: 'text',
    createdAt: date
  }
}

export const groupRetrievedFirebase = (
  date: Date
): IMessageGroupRetrievedFirebase => {
  return {
    id: uniqueUID(),
    createdAt: new firebase.firestore.Timestamp(testDate.getTime() / 1000, 0),
    updatedAt: new firebase.firestore.Timestamp(testDate.getTime() / 1000, 0),
    recentMessage: messageRetrievedFirebase(date),
    ...baseGroup
  }
}

export const getGroup = (date: Date): IMessageGroup => {
  return {
    id: uniqueUID(),
    createdAt: date,
    updatedAt: date,
    recentMessage: getMessage(date),
    ...baseGroup
  }
}
