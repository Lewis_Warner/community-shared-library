import {
  IMessageCreate,
  IMessageGroup,
  IMessageGroupUpdate
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { ChatDaoClient } from '../daos/chat/chat_client_dao'
import { traverseGroupMembers } from '../helpers/chat/chat_helper'
export const sendMessage = async (
  message: IMessageCreate,
  group: IMessageGroup
): Promise<void> => {
  const chatDao = new ChatDaoClient()
  const groupUpdate: IMessageGroupUpdate = {
    ...group,
    recentMessage: message,
    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
  }
  await chatDao.addMessage(message, groupUpdate.id)
  // update the group with relevant update data per message
  const recipients: string[] = []
  traverseGroupMembers(groupUpdate, (uid) => {
    if (uid !== message.senderId) {
      recipients.push(uid)
    }
  })
  return chatDao.updateGroupAfterMessage(groupUpdate, recipients)
}
