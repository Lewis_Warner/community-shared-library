/* eslint-disable no-param-reassign */

import {
  IChatState,
  IMessageGroup
} from '@warnster/community-shared-interfaces'
import produce from 'immer'
import { ChatDaoClient } from '../../daos/chat/chat_client_dao'
import {
  ADD_GROUP_SELECTED,
  APPEND_MESSAGE,
  GET_GROUPS,
  PREPEND_MESSAGES,
  REMOVE_GROUPS,
  SET_GROUPS,
  SET_MESSAGES,
  SET_SELECTED_GROUP,
  UPDATE_GROUPS,
  UPDATE_GROUPS_LOADED
} from '../actions/chatActions'

export const chatInitialState: IChatState = {
  groups: [],
  selectedGroup: null,
  messages: [],
  groupsLoaded: false
}

export const chatReducer = (
  state = chatInitialState,
  action: any
): IChatState => {
  switch (action.type) {
    case SET_SELECTED_GROUP: {
      const { selectedGroup } = action.payload
      return produce(state, (draft) => {
        draft.selectedGroup = selectedGroup
      })
    }

    case ADD_GROUP_SELECTED: {
      const { group }: { group: IMessageGroup } = action.payload
      return produce(state, (draft) => {
        draft.groups.push(group)
        draft.selectedGroup = group
      })
    }

    case GET_GROUPS: {
      return produce(state, (draft) => {
        draft.groups = action.payload.groups
      })
    }

    case UPDATE_GROUPS: {
      return produce(state, (draft) => {
        // merge both arrays and sort by updated at
        const groups: IMessageGroup[] = action.payload.groups.concat(
          state.groups
        )
        const uniqueGroups = groups.filter(
          (group, index, self) =>
            index === self.findIndex((g) => g.id === group.id)
        )
        uniqueGroups.sort(
          (a, b) => b.updatedAt.getTime() - a.updatedAt.getTime()
        )
        draft.groups = uniqueGroups
        if (uniqueGroups.length === action.payload.userGroupLength) {
          draft.groupsLoaded = true
        } else {
          draft.groupsLoaded = false
        }
      })
    }

    case REMOVE_GROUPS: {
      return produce(state, (draft) => {
        const newGroups = state.groups.filter((g) => {
          if (!action.payload.groupIDs.includes(g.id)) {
            return true
          }
          return false
        })
        newGroups.sort((a, b) => b.updatedAt.getTime() - a.updatedAt.getTime())
        draft.groups = newGroups
      })
    }

    case SET_GROUPS: {
      return produce(state, (draft) => {
        draft.groups = action.payload.groups
        draft.groupsLoaded = false
      })
    }

    case SET_MESSAGES: {
      return produce(state, (draft) => {
        draft.messages = action.payload.messages
      })
    }

    case UPDATE_GROUPS_LOADED: {
      return produce(state, (draft) => {
        draft.groupsLoaded = action.payload.groupsLoaded
      })
    }

    case APPEND_MESSAGE: {
      return produce(state, (draft) => {
        const newMessage = action.payload.message
        const lastMessage = draft.messages[draft.messages.length - 1]
        if (!lastMessage) {
          console.error(
            'error with last message being undefined',
            draft.messages
          )
        }
        if (lastMessage && newMessage.id !== lastMessage.id) {
          draft.messages.push(action.payload.message)
          if (newMessage.senderId !== action.payload.userID) {
            const chatDao = new ChatDaoClient()
            chatDao.updateGroupUnread(
              action.payload.groupID,
              action.payload.userID
            )
          }
        }
      })
    }

    case PREPEND_MESSAGES: {
      return produce(state, (draft) => {
        draft.messages = action.payload.messages.concat(draft.messages)
      })
    }

    default: {
      return state
    }
  }
}
