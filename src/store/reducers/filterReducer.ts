import { IFilterState } from '@warnster/community-shared-interfaces'
import produce from 'immer'
import { SET_PROFILE_FILTERS } from '../actions/filterActions'

export const filterInitialState: IFilterState = {
  profileFilters: {
    filters: {},
    limit: 24,
    page: 1,
    sort: {
      last_changed: -1
    }
  }
}

export const filterReducer = (
  state = filterInitialState,
  action: any
): IFilterState => {
  switch (action.type) {
    case SET_PROFILE_FILTERS: {
      const { profileFilters } = action.payload
      return produce(state, (draft) => {
        draft.profileFilters = profileFilters
      })
    }
    default: {
      return state
    }
  }
}
