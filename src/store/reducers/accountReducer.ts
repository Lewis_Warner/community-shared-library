import { IUser, IUserState } from '@warnster/community-shared-interfaces'
import produce from 'immer'
import {
  LOGIN_SUCCESS,
  SET_PROFILE,
  SET_PROFILE_IMAGES,
  SET_USER,
  SILENT_LOGIN,
  UPDATE_USER_GROUP
} from '../actions/accountActions'

export const userInitialState: IUserState = {
  user: {
    uid: '',
    email: '',
    groups: [],
    profileID: '',
    phone: '',
    isDeleted: false,
    profileComplete: false,
    isAdmin: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    usage: {
      messages: 0,
      profileViews: 0
    }
  },
  loggedIn: false,
  profile: {
    images: [],
    profileID: '',
    name: '',
    avatar: '',
    userID: '',
    isComplete: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    gender: null,
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    dateOfBirth: null,
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    genre: null,
    description: '',
    city: '',
    county: '',
    country: '',
    geohash: ''
  }
}

export const accountReducer = (
  state = userInitialState,
  action: any
): IUserState => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const { user } = action.payload

      return produce(state, (draft) => {
        draft.user = user
      })
    }

    case SET_USER: {
      const { user }: { user: IUser } = action.payload

      return produce(state, (draft) => {
        draft.user = user
      })
    }

    case SILENT_LOGIN: {
      const { user, loggedIn } = action.payload
      return produce(state, (draft) => {
        draft.user = user
        draft.loggedIn = loggedIn
      })
    }

    case UPDATE_USER_GROUP: {
      const { groupID }: { groupID: string } = action.payload
      return produce(state, (draft) => {
        draft.user.groups.push(groupID)
      })
    }

    case SET_PROFILE: {
      return produce(state, (draft) => {
        draft.profile = action.payload.profile
      })
    }

    case SET_PROFILE_IMAGES: {
      return produce(state, (draft) => {
        draft.profile.images = action.payload.images
      })
    }

    default: {
      return state
    }
  }
}
