import { IProfileFilters } from '@warnster/community-shared-interfaces'

export const SET_PROFILE_FILTERS = '@filters/profile/set-filters'

export function setProfileFilters(
  profileFilters: IProfileFilters
): {
  type: string
  payload: {
    profileFilters: IProfileFilters
  }
} {
  return {
    type: SET_PROFILE_FILTERS,
    payload: { profileFilters }
  }
}
