import {
  IImage,
  IProfile,
  IProfileUpdateDraft,
  IUser
} from '@warnster/community-shared-interfaces'
import { RealtimeDao } from '../../daos/firebase/realtime_dao'
import { StorageDaoClient } from '../../daos/firebase/storage'
import { ProfileDaoClient } from '../../daos/profile/profile_client_dao'
import { UserDaoClient } from '../../daos/user/user_client_dao'
import { IAppState } from '../../interfaces/settings_interface'

export const LOGIN_SUCCESS = '@account/login-success'
export const SILENT_LOGIN = '@account/silent-login'
export const LOGOUT = '@account/logout'
export const UPDATE_USER_GROUP = '@account/update-user-group'
export const SET_USER = '@account/set-user'
export const SET_PROFILE = '@account/profile/set'
export const SET_PROFILE_IMAGES = '@account/profile/image/set'

export function login(user: IUser) {
  return (dispatch: any): void =>
    dispatch({
      type: SILENT_LOGIN,
      payload: {
        user,
        loggedIn: true
      }
    })
}

export function setUser(
  user: IUser
): {
  type: string
  payload: {
    user: IUser
  }
} {
  return {
    type: SET_USER,
    payload: { user }
  }
}

export function appendUserGroup(
  groupID: string
): {
  type: string
  payload: {
    groupID: string
  }
} {
  return {
    type: UPDATE_USER_GROUP,
    payload: { groupID }
  }
}

export function logout(): (dispatch: any, getState: any) => Promise<void> {
  return async (dispatch: any, getState: any): Promise<void> => {
    const state: IAppState = getState()
    try {
      const realtimeDao = new RealtimeDao()
      await realtimeDao.setUserOffline(state.account.user.uid)
    } catch (err) {
      console.error(err)
    }
    try {
      const userDao = new UserDaoClient()
      await userDao.firebaseLogout()
    } catch (err) {
      console.error(err)
    } finally {
      dispatch({
        type: LOGOUT
      })
    }
  }
}

export function setProfile(
  profile: IProfile
): {
  type: string
  payload: {
    profile: IProfile
  }
} {
  return {
    type: SET_PROFILE,
    payload: { profile }
  }
}

export function updateProfile(
  profileUpdate: IProfileUpdateDraft,
  profileID: string
) {
  return (dispatch: any, getState: any): void => {
    const profileDao = new ProfileDaoClient()
    const state: IAppState = getState()
    const { profile } = state.account
    const { imagesNew } = profileUpdate
    delete profileUpdate.imagesNew
    profileDao.updateProfile(profileUpdate, profileID)
    let images: IImage[] = profile.images
    if (imagesNew) {
      images = [...profile.images, ...imagesNew]
    }
    
    const newProfile: IProfile = {
      ...profile,
      ...profileUpdate,
      updatedAt: new Date(),
      images,
      last_changed: new Date()
    }
    dispatch(setProfile(newProfile))
  }
}

export function removeProfileImage(
  image: IImage
): (dispatch: any, getState: any) => void {
  const storageDao = new StorageDaoClient()
  const profileDao = new ProfileDaoClient()
  return (dispatch: any, getState: any): void => {
    const state: IAppState = getState()
    const { profile } = state.account
    storageDao.deleteFile(image.path)
    const images = profile.images.filter((i) => i.path !== image.path)
    const profileUpdate: IProfileUpdateDraft = {
      images
    }
    if (image.url === profile.avatar) {
      if (profile.images.length > 0) {
        profileUpdate.avatar = profile.images[0].url
      } else {
        profileUpdate.avatar = ''
      }
    }
    // update profile,
    profileDao.updateProfile(profileUpdate, profile.profileID)
    dispatch({
      type: SET_PROFILE_IMAGES,
      payload: { images }
    })
  }
}
