import {
  IMessage,
  IMessageCreate,
  IMessageGroup,
  IMessageGroupCreate,
  IMessageGroupRetrievedFirebase
} from '@warnster/community-shared-interfaces'
import { Dispatch } from 'redux'
import { ChatHelper } from '../..'
import { ChatDaoClient } from '../../daos/chat/chat_client_dao'
import { IAppState } from '../../interfaces/settings_interface'
export const GET_GROUPS = '@chat/get-groups'
export const SET_SELECTED_GROUP = '@chat/set-selected-group'
export const ADD_GROUP_SELECTED = '@chat/add-group-selected'
export const SET_GROUPS = '@chat/set-groups'
export const SET_MESSAGES = '@chat/set-messages'
export const APPEND_MESSAGE = '@chat/append-message'
export const PREPEND_MESSAGES = '@chat/prepend-messages'
export const UPDATE_GROUPS = '@chat/update-groups'
export const REMOVE_GROUPS = '@chat/remove-groups'
export const UPDATE_GROUPS_LOADED = '@chat/update-groups-loaded'

export function updateGroups(
  groups: IMessageGroup[]
): (dispatch: Dispatch, getState: any) => void {
  return (
    dispatch: Dispatch,
    getState: any
  ): {
    type: string
    payload: { groups: IMessageGroup[]; userGroupLength: number }
  } => {
    const state: IAppState = getState()
    return dispatch({
      type: UPDATE_GROUPS,
      payload: { groups, userGroupLength: state.account.user.groups.length }
    })
  }
}

export function updateGroupsLoaded(
  groupsLoaded: boolean
): {
  type: string
  payload: {
    groupsLoaded: boolean
  }
} {
  return {
    type: UPDATE_GROUPS_LOADED,
    payload: { groupsLoaded }
  }
}

export function removeGroups(
  groupIDs: string[]
): {
  type: string
  payload: {
    groupIDs: string[]
  }
} {
  return {
    type: REMOVE_GROUPS,
    payload: { groupIDs }
  }
}

export function setGroups(
  groups: IMessageGroup[]
): {
  type: string
  payload: {
    groups: IMessageGroup[]
  }
} {
  return {
    type: SET_GROUPS,
    payload: { groups }
  }
}

export function setSelectedGroup(
  selectedGroup: IMessageGroup | null
): {
  type: string
  payload: {
    selectedGroup: IMessageGroup | null
  }
} {
  // check users are in local GroupUsers state, if not then find the users and dispatch the selectGroup.
  // This will ensure that a group that is selected will always have the recipient users in there.
  return {
    type: SET_SELECTED_GROUP,
    payload: { selectedGroup }
  }
}

export function createGroup(
  group: IMessageGroupCreate,
  message: IMessageCreate
): (
  dispatch: Dispatch
) => Promise<{
  type: string
  payload: {
    selectedGroup: IMessageGroup | null
  }
} | null> {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  return async (dispatch: Dispatch) => {
    try {
      const chatDao = new ChatDaoClient()
      const groupDocRef = await chatDao.createGroup(group)

      await chatDao.addMessage(message, groupDocRef.id)

      chatDao.updateGroup(groupDocRef.id, { id: groupDocRef.id })

      const groupData = await ChatHelper.transformFirebaseGroup(
        (await groupDocRef.get()).data() as IMessageGroupRetrievedFirebase,
        groupDocRef.id
      )
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      dispatch(updateGroups([groupData]))
      return dispatch(setSelectedGroup(groupData))
    } catch (err) {
      console.error(err)
      return null
    }
  }
}

export const setMessages = (
  messages: IMessage[]
): {
  type: string
  payload: {
    messages: IMessage[]
  }
} => {
  return {
    type: SET_MESSAGES,
    payload: { messages }
  }
}

export const appendMessage = (
  message: IMessage,
  groupID: string,
  userID: string
): {
  type: string
  payload: {
    message: IMessage
    groupID: string
    userID: string
  }
} => {
  return {
    type: APPEND_MESSAGE,
    payload: { message, groupID, userID }
  }
}

export const prependMessages = (
  messages: IMessage[]
): {
  type: string
  payload: {
    messages: IMessage[]
  }
} => {
  return {
    type: PREPEND_MESSAGES,
    payload: { messages }
  }
}
