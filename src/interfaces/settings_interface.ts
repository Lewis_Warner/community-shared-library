import {
  IChatState,
  IFilterState,
  IUserState
} from '@warnster/community-shared-interfaces'
import { DefaultRootState } from 'react-redux'

export interface IAppState extends DefaultRootState {
  account: IUserState
  chat: IChatState
  filters: IFilterState
}

export interface IFileDisplay extends File {
  url?: string
}
