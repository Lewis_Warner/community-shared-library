import axios, { AxiosInstance, AxiosResponse } from 'axios'
import { UserDaoClient } from '../daos/user/user_client_dao'

class BaseRequest {
  httpService: AxiosInstance

  constructor() {
    this.httpService = axios.create()

    this.httpService.interceptors.response.use(
      (response: any) => {
        return Promise.resolve(response)
      },
      (error: any) => {
        if (error.response) {
          const errors = (error.response.data || {}).errors || []
          error.response.customError = errors.length
            ? errors[0]
            : 'An error occurred'
          return Promise.reject(error.response)
        } else {
          return Promise.reject(error)
        }
      }
    )
  }

  get<T = any>(path: string, params?: any): Promise<AxiosResponse<T>> {
    return this.httpService.get<T>(path, params)
  }

  patch<T = any>(path: string, payload?: any): Promise<AxiosResponse<T>> {
    return this.httpService.request<T>({
      method: 'PATCH',
      url: path,
      responseType: 'json',
      data: payload || {}
    })
  }

  put<T = any>(path: string, payload?: any): Promise<AxiosResponse<T>> {
    return this.httpService.request<T>({
      method: 'PUT',
      url: path,
      responseType: 'json',
      data: payload || {}
    })
  }

  post<T = any>(path: string, payload?: any): Promise<AxiosResponse<T>> {
    return this.httpService.request<T>({
      method: 'POST',
      url: path,
      responseType: 'json',
      data: payload || {}
    })
  }

  delete<T = any>(path: string, payload?: any): Promise<AxiosResponse<T>> {
    return this.httpService.request<T>({
      method: 'DELETE',
      url: path,
      responseType: 'json',
      data: payload || {}
    })
  }

  async all(requests: any): Promise<unknown[]> {
    return axios.all(requests)
  }
}

class SecureRequest extends BaseRequest {
  constructor() {
    super()
    this.httpService.interceptors.request.use(
      async (config: any): Promise<any> => {
        const userDao = new UserDaoClient()
        const user = await userDao.getCurrentUser()
        const token = await user?.getIdToken()
        config.headers.common['x-access-token'] = token || ''
        return config
      }
    )

    this.httpService.interceptors.response.use(undefined, (error: any) => {
      if (error.status === 403) {
        throw Error(error)
      }

      return Promise.reject(error)
    })
  }
}

export const http = new BaseRequest()
export const secureHttp = new SecureRequest()
