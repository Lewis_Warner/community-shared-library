import { v4 as uuid } from 'uuid'

export const getInitials = (name = ''): string => {
  return name
    .replace(/\s+/, ' ')
    .split(' ')
    .slice(0, 2)
    .map((v) => v && v[0].toUpperCase())
    .join('')
}

export const bytesToSize = (bytes: number, decimals = 2): string => {
  if (bytes === 0) return '0 Bytes'

  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}

export const getBase64 = async (
  file: File
): Promise<string | ArrayBuffer | null | undefined> => {
  const reader = new FileReader()
  return new Promise((resolve) => {
    reader.onload = (e): void => {
      resolve(e?.target?.result)
    }
    reader.readAsDataURL(file)
  })
}

const objFromArray = (arr: any, key = 'id'): any =>
  arr.reduce((accumulator: any, current: any) => {
    accumulator[current[key]] = current
    return accumulator
  }, {})

export default objFromArray

export const uniqueUID = (): string => {
  return uuid()
}
