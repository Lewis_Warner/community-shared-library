import {
  IComment,
  ICommentFirebase,
  IPost,
  IPostFirebase
} from '@warnster/community-shared-interfaces'

export const transformFirebasePost = (
  firebasePost: IPostFirebase,
  postID: string
): IPost => {
  const post: IPost = {
    ...firebasePost,
    createdAt: firebasePost.createdAt.toDate(),
    postID,
    comments: []
  }
  return post
}
export const transformFirebaseComment = (
  firebaseComment: ICommentFirebase,
  commentID: string
): IComment => {
  const comment: IComment = {
    ...firebaseComment,
    createdAt: firebaseComment.createdAt.toDate(),
    commentID
  }
  return comment
}
