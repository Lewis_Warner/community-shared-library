import {
  EGroupType,
  IGroupMemberData,
  IMessage,
  IMessageCreate,
  IMessageGroup,
  IMessageGroupCreate,
  IMessageGroupRetrievedFirebase,
  IMessageGroupRetrievedFirebasePending,
  IMessageRetrievedFirebase,
  IMessageRetrievedFirebasePending,
  IProfile,
  TMessageContentType,
  TMessageGroup
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { StorageDaoClient } from '../../daos/firebase/storage'
import { uniqueUID } from '../utils'
const storageDao = new StorageDaoClient()
export const transformFirebaseMessage = async (
  messageRetrieved: IMessageRetrievedFirebase | IMessageRetrievedFirebasePending
): Promise<IMessage> => {
  if (messageRetrieved.contentType === 'image') {
    const url = await storageDao
      .getFileReference(messageRetrieved.body)
      .getDownloadURL()
    messageRetrieved.body = url
  }
  return {
    ...messageRetrieved,
    createdAt: messageRetrieved.createdAt
      ? messageRetrieved.createdAt.toDate()
      : new Date()
  }
}

export const transformFirebaseGroup = async (
  groupRetrieved:
    | IMessageGroupRetrievedFirebase
    | IMessageGroupRetrievedFirebasePending,
  groupID?: string
): Promise<IMessageGroup> => {
  return {
    ...groupRetrieved,
    createdAt: groupRetrieved.createdAt
      ? groupRetrieved.createdAt.toDate()
      : new Date(),
    updatedAt: groupRetrieved.updatedAt
      ? groupRetrieved.updatedAt.toDate()
      : new Date(),
    recentMessage: await transformFirebaseMessage(groupRetrieved.recentMessage),
    id: groupRetrieved.id ? groupRetrieved.id : groupID || ''
  }
}

export const traverseGroupMembers = (
  group: TMessageGroup,
  callback: (userID: string, userData: IGroupMemberData) => void
): void => {
  for (const [key, value] of Object.entries(
    group.members ? group.members : {}
  )) {
    callback(key, value)
  }
}

export const getGroupAvatarAndName = (
  group: IMessageGroup,
  userID?: string //logged in user
): { avatar: string; name: string } => {
  let toolbarInfoTmp: any = { name: group.name, avatar: group.avatar }
  if (group.type === EGroupType.PrivateChat && userID) {
    traverseGroupMembers(group, (uid, userData) => {
      if (uid !== userID) {
        toolbarInfoTmp = { name: userData.name, avatar: userData.avatar }
      }
    })
  }
  return toolbarInfoTmp
}

export const calculateTotalUnreadMessages = (
  groups: IMessageGroup[],
  userID: string
): number => {
  let totalUnread = 0
  groups.forEach((group) => {
    if (Object.prototype.hasOwnProperty.call(group.unread, 'userID')) {
      totalUnread += group.unread[userID]
    }
  })
  return totalUnread
}

export const findPrivateChatByUserID = (
  groups: IMessageGroup[],
  senderID: string,
  recipientID: string
): IMessageGroup | undefined => {
  return groups.find((group) => {
    if (
      group.type === EGroupType.PrivateChat &&
      group.members[senderID] &&
      group.members[recipientID]
    ) {
      return true
    } else {
      return false
    }
  })
}

export const getFormattedMessages = async (
  snapshots: firebase.firestore.QuerySnapshot
): Promise<IMessage[]> => {
  const messages: IMessage[] = []
  for (let i = 0; i < snapshots.docs.length; i++) {
    const doc = snapshots.docs[i]
    let message: IMessage
    if (doc.metadata.hasPendingWrites) {
      message = await transformFirebaseMessage(
        doc.data() as IMessageRetrievedFirebasePending
      )
    } else {
      message = await transformFirebaseMessage(
        doc.data() as IMessageRetrievedFirebase
      )
    }
    messages.push(message)
  }
  return messages.reverse()
}

export const createMessage = (
  body: string,
  senderId: string,
  type: TMessageContentType
): IMessageCreate => {
  return {
    body: body,
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    senderId: senderId,
    contentType: type,
    id: uniqueUID()
  }
}

// clear
export const createGroupObjects = (
  groupRecipients: IProfile[],
  message: IMessageCreate,
  userID: string,
  groupName = 'Private Group Chat'
): { newGroupData: IMessageGroupCreate } => {
  const unread: { [key: string]: number } = {}
  const members: { [key: string]: IGroupMemberData } = {}
  groupRecipients.forEach((profile) => {
    if (profile.userID !== userID) {
      unread[profile.userID] = 1
    } else {
      unread[profile.userID] = 0
    }
    members[profile.userID] = {
      name: profile.name,
      avatar: profile.avatar,
      isRemoved: false,
      isDeleted: false
    }
  })
  const groupType =
    groupRecipients.length > 2
      ? EGroupType.InviteOnlyGroupChat
      : EGroupType.PrivateChat
  const newGroupData: IMessageGroupCreate = {
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    createdBy: userID,
    members,
    name: groupName,
    type: groupType,
    avatar: '',
    unread,
    recentMessage: message,
    updatedAt: firebase.firestore.FieldValue.serverTimestamp()
  }
  return { newGroupData }
}

/**
 *
 * @param profiles
 * @param group
 * @returns a string key of the object values to update
 */
export const addUsersToGroup = (
  profiles: IProfile[],
  group: IMessageGroup
): any => {
  const updateData: any = {}
  profiles.forEach((profile) => {
    let wasRemoved = false
    // find users that have removed flag and add them to updateData
    for (const [userID, userData] of Object.entries(group.members)) {
      if (userID === profile.userID && userData.isRemoved === true) {
        updateData[`members.${profile.userID}.isRemoved`] = false
        updateData[
          `unread.${profile.userID}`
        ] = firebase.firestore.FieldValue.increment(1)
        wasRemoved = true
      }
    }
    // If was not previously removed then add user like normal
    if (wasRemoved === false) {
      const memberData: IGroupMemberData = {
        name: profile.name,
        isDeleted: false,
        isRemoved: false,
        avatar: profile.avatar
      }
      updateData[`members.${profile.userID}`] = memberData
      updateData[`unread.${profile.userID}`] = 1
    }
  })
  return updateData
}
