import {
  EGroupType,
  IMessageRetrievedFirebase,
  IMessageRetrievedFirebasePending
} from '@warnster/community-shared-interfaces'
import { ChatHelper } from '../../..'
import {
  getGroup,
  groupRetrievedFirebase,
  messageRetrievedFirebase,
  TEST_USER_ID_1,
  TEST_USER_ID_2
} from '../../../test-data/chat-test-data'
const downloadUrl = 'download url'
// jest.mock('../../../daos/firebase/storage', () => ({
//   StorageDaoClient: function (): any {
//     return {
//       getFileReference: (): any => ({
//         getDownloadURL: (): string => downloadUrl
//       })
//     }
//   }
// }))
describe('chat helper', () => {
  const testDate = new Date()

  const message = messageRetrievedFirebase(testDate)
  it('should transform firebase recieved text message', async () => {
    const result = await ChatHelper.transformFirebaseMessage(message)
    expect({ ...result, createdAt: null }).toEqual({
      ...message,
      createdAt: null
    })
    expect(result.createdAt).toEqual(testDate)
  })

  it('should transform firebase pending text message', async () => {
    const pendingMessage: IMessageRetrievedFirebasePending = {
      ...message,
      createdAt: null
    }
    const result = await ChatHelper.transformFirebaseMessage(pendingMessage)
    expect(pendingMessage).toEqual({ ...result, createdAt: null })
    expect(result.createdAt).toBeInstanceOf(Date)
  })

  it('should transform firebase recieved image message', async () => {
    const recievedMessage: IMessageRetrievedFirebase = {
      ...message,
      contentType: 'image'
    }
    const result = await ChatHelper.transformFirebaseMessage(recievedMessage)
    expect(result.body).toEqual(downloadUrl)
  })

  it('should transform firebase recieved group', async () => {
    const groupRetrieved = groupRetrievedFirebase(testDate)
    const group = await ChatHelper.transformFirebaseGroup(groupRetrieved)
    expect({
      ...group,
      createdAt: null,
      updatedAt: null,
      recentMessage: null
    }).toEqual({
      ...groupRetrieved,
      createdAt: null,
      updatedAt: null,
      recentMessage: null
    })
    expect(groupRetrieved.createdAt.toDate()).toEqual(group.createdAt)
  })

  it('should get group avatar and name from private group', () => {
    const group = getGroup(testDate)
    const result = ChatHelper.getGroupAvatarAndName(group, TEST_USER_ID_1)
    expect(result.avatar).toEqual(group.members[TEST_USER_ID_2].avatar)
    expect(result.name).toEqual(group.members[TEST_USER_ID_2].name)
  })

  it('should get group avatar and name from private group', () => {
    const group = {
      ...getGroup(testDate),
      type: EGroupType.InviteOnlyGroupChat
    }
    const result = ChatHelper.getGroupAvatarAndName(group)
    expect(result.avatar).toEqual(group.avatar)
    expect(result.name).toEqual(group.name)
  })
})
