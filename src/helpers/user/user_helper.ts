import {
  IImage,
  IProfile,
  IProfileFirebase,
  IUser,
  IUserRetrievedFirebase
} from '@warnster/community-shared-interfaces'
import firebase from 'firebase'
import { StorageDaoClient } from '../../daos/firebase/storage'

export const transformFirebaseUser = (
  firebaseUser: IUserRetrievedFirebase
): IUser => {
  return {
    ...firebaseUser,
    createdAt: firebaseUser.createdAt.toDate(),
    updatedAt: firebaseUser.updatedAt.toDate()
  }
}

export const getImages = async (images: IImage[]): Promise<IImage[]> => {
  const storageDao = new StorageDaoClient()
  for (let i = 0; i < images.length; i++) {
    // getting the larget image by default
    const image = images[i]
    const url800 = (await storageDao
      .getFileReference(image.path + '_800x800.jpeg')
      .getDownloadURL()) as string
    const url400 = (await storageDao
      .getFileReference(image.path + '_400x400.jpeg')
      .getDownloadURL()) as string
    const url200 = (await storageDao
      .getFileReference(image.path + '_200x200.jpeg')
      .getDownloadURL()) as string
    images[i] = {
      path: image.path,
      url: url800,
      sizes: {
        200: url200,
        400: url400,
        800: url800
      }
    }
  }
  return images
}

export const getAvatar = (images: IImage[]): string => {
  let avatar = ''
  if (avatar === '' && images.length > 0) {
    avatar = images[0].sizes[200]
  }
  return avatar
}
export const transformFirebaseProfile = async (
  firebaseProfile: IProfileFirebase,
  profileID: string
): Promise<IProfile> => {
  const storageDao = new StorageDaoClient()
  let images: IImage[] = []
  let avatar = firebaseProfile.avatar || ''
  if (storageDao.storageAvailable === true) {
    images = await getImages(firebaseProfile.images)
    if (avatar === '' && firebaseProfile.images.length > 0) {
      avatar = getAvatar(firebaseProfile.images)
    }
  } else {
    images = firebaseProfile.images
  }
  const profile: Partial<IProfile> = {
    ...firebaseProfile,
    createdAt: firebaseProfile.createdAt.toDate(),
    updatedAt: firebaseProfile.updatedAt.toDate(),
    profileID,
    avatar,
    last_changed: firebaseProfile.last_changed.toDate(),
    images,
    dateOfBirth: undefined
  }
  // date of birth will not always be available if profile form not filled out
  if (firebaseProfile.dateOfBirth) {
    profile.dateOfBirth = firebaseProfile.dateOfBirth.toDate()
  }
  return profile as IProfile
}

export const isFacebookOrTwitter = (firebaseUser: firebase.User): boolean => {
  const result = firebaseUser.providerData.find(
    (d) =>
      d && (d.providerId === 'facebook.com' || d.providerId === 'twitter.com')
  )
  return !!result
}

export const CurrencyToSymbol: { [userID: string]: string } = {
  gbp: '£',
  usd: '$'
}
