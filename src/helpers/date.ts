import moment from 'moment'

export const getMoment = (date: string | Date): moment.Moment => {
  return moment(date).local()
}

export const getMomentFromUnix = (timestamp: number): moment.Moment => {
  return moment.unix(timestamp)
}

export const formatAppDate = (date: moment.Moment): string => {
  return date.format('dddd, MMMM Do YYYY, h:mm:ss a')
}

export const formatShortAppDate = (date: moment.Moment): string => {
  return date.format('DD/MM/YY')
}

export const fromNow = (date: moment.Moment): string => {
  return date.fromNow()
}

export const getMinimumAgeDate = (): Date => {
  const minDate = moment().subtract(18, 'years').toDate()
  return minDate
}

export const getAge = (date: moment.Moment): string => {
  return date.fromNow(true).split(' ')[0]
}
